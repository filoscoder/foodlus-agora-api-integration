import { UnauthorizedError } from "@/helpers";
import type { Request, Response, NextFunction } from "express";
import CONFIG from "@/config";

export const auth = (
    req: Request,
    _res: Response,
    next: NextFunction
): void => {
    const authorization = req.headers.authorization || "";
    const token = authorization?.split("Bearer ")[1];
    const isAuthorized = token === CONFIG.SERVER.SECRET_TOKEN;

    if (!token || !isAuthorized) {
        throw new UnauthorizedError();
    }

    next();
};
