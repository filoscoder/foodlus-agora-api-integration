import CONFIG from "@/config";
import axios, { AxiosRequestConfig } from "axios";

const { AGORA } = CONFIG;
const { BASE_URL, API_TOKEN } = AGORA;

const baseInstance = axios.create({
    baseURL: `${BASE_URL}/api`,
    headers: {
        ["Api-Token"]: API_TOKEN,
        Accept: "application/json",
        ["Accept-Encoding"]: "gzip",
        ["Content-Type"]: "application/json; charset=utf-8"
    }
});

baseInstance.interceptors.response.use((res) => res.data);

type ResponseData<T = undefined> = false extends (
    T extends undefined ? true : false
)
    ? {
          data: T;
      }
    : null;

const apiRequest = {
    get: <T = undefined>(url: string, request?: AxiosRequestConfig) =>
        baseInstance.get<T, ResponseData<T>>(url, request),
    delete: <T = undefined>(url: string, request?: AxiosRequestConfig) =>
        baseInstance.delete<T, ResponseData<T>>(url, request),
    post: <T = undefined>(
        url: string,
        request?: unknown,
        config?: AxiosRequestConfig
    ) => baseInstance.post<T, ResponseData<T>>(url, request, config),
    put: <T = undefined>(
        url: string,
        request?: unknown,
        config?: AxiosRequestConfig
    ) => baseInstance.put<T, ResponseData<T>>(url, request, config),
    patch: <T = undefined>(
        url: string,
        request?: unknown,
        config?: AxiosRequestConfig
    ) => baseInstance.patch<T, ResponseData<T>>(url, request, config)
};

export default apiRequest;
