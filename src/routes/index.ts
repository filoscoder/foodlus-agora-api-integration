import { revoRoutes } from "@/components/agora/agora.routes";
import { auth } from "@/middlewares/auth";
import { Router } from "express";

export const routes = Router();

routes.use(auth, revoRoutes);

export default routes;
