import type { Application } from "express";
import CONFIG from "./config";

export const startServer = (app: Application) => {
    const httpServer = app;

    httpServer.listen({ port: CONFIG.APP.PORT }, () => {
        process.stdout.write(
            `✅ [FOODLUS-API][${CONFIG.APP.ENV}] Started on: ${Date.now()}\n`
        );
        process.stdout.write(
            `🚀 Server listening at: http://${CONFIG.APP.HOST}:${CONFIG.APP.PORT}\n`
        );
    });
};
