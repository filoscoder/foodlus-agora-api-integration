import HttpStatus, { NOT_FOUND } from "http-status/lib";

export class NotFoundException extends Error {
    public readonly statusCode: number;
    public readonly message: string;

    constructor() {
        super();
        this.statusCode = NOT_FOUND;
        this.message = HttpStatus[NOT_FOUND];

        Error.captureStackTrace(this);
    }
}
