import HttpStatus, { UNAUTHORIZED } from "http-status/lib";

export class UnauthorizedError extends Error {
    public readonly statusCode: number;
    public readonly message: string;

    constructor() {
        super();
        this.statusCode = UNAUTHORIZED;
        this.message = HttpStatus[UNAUTHORIZED];

        Error.captureStackTrace(this);
    }
}
