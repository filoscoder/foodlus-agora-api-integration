export interface AgoraProduct {
    Id: number;
    Name: string; // "Whisky DYC";
    VatId: number;
    BaseSaleFormatId: number;
    FamilyId: number;
    ButtonText: string; // "Copa";
    PLU: string; // "0001"
    Color: `#${string}`; // "#341122"
    Barcodes: Barcode[];
    StorageOptions: StorageOption[];
    Order: number;
    UseAsDirectSale: boolean;

    SaleableAsMain: boolean;
    SaleableAsAddin: boolean;
    AskForAddins: boolean;
    AskForPreparationNotes: boolean;
    PrintWhenPriceIsZero: boolean;
    PreparationTypeId: number;
    PreparationOrderId: number;
    IsSoldByWeight: boolean;
    Prices: Price[];
    AddinRoles: AddinRole[];
    AdditionalSaleFormats: AdditionalSaleFormat[];
    CostPrices: CostPrice[];
}

interface Barcode {
    BarcodeValue: string; // "15998763"
}

interface Price {
    PriceListId: number; // 10
    Price: number; // "3.00"
    AddinPrice: number; // "0.00"
    MenuItemPrice: number; // "0.00"
}

interface CostPrice {
    WarehouseId: number; // 1
    CostPrice: number; // "2.3"
}

interface StorageOption {
    WarehouseId: number;
    Location: string; // "S1.A90"
    MinStock: number;
    MaxStock: number;
}

interface AddinRole {
    Name: string;
    ButtonText: string; // "Copa";
    Color: `#${string}`; // "#341122"
    MinAddins: number;
    MaxAddins: number;
    UsePreparationType: number;
}

interface AdditionalSaleFormat {
    Id: number;
    Name: string;
    Ratio: number;
    Color: `#${string}`; // "#341122"
    SaleableAsMain: boolean;
    SaleableAsAddin: boolean;
    AskForAddins: boolean;
    Prices: Price[];
    AddinRoles: AddinRole[];
}
