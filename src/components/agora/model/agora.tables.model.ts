export interface AgoraSaleCenter {
    Id: number;
    Name: string; // "Terraza"
    PriceListId: number;
    CurrentPriceListId: number;
    VatIncluded: boolean;
    ButtonText: string; // "ROOF"
    Color: `#${string}`; // "#341122"
    StartTakeOutOrder: boolean;
    GuestProductId: number;
    WhenAskForGuests: "Never" | "OnOpenDocument" | "OnCloseDocument";
    WhenAskForFriendlyName: "Never" | "OnOpenTicket" | "OnCloseTicket";
    SaleLocations: SaleLocation[];
}

export interface SaleLocation {
    Name: string; // "S1"
}
