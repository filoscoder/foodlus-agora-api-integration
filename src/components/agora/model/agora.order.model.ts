import { AgoraSaleCenter, SaleLocation } from "./agora.tables.model";

export interface AgoraSalesOrders {
    Serie: string; // "P"
    Number: number;
    Type: "Standard" | "Delivery" | "TakeAway" | "Table";
    BusinessDay: string; // "aaaa-mm-dd";
    Guests: null;
    VatIncluded: boolean;
    Date: string; // "aaaa-mm-ddThh:mm:ss";
    // DeliveryDate?: string; // "aaaa-mm-ddThh:mm:ss";
    // DeliveryAddress?;
    Pos: Identifier;
    Workplace: Identifier;
    User: Identifier;
    SaleCenter: SaleCenter;
    Status: "Pending" | "Cancelled" | "Invoiced";
    AutoPrepare?: "OnlyIfNew"; // Enviar comandas sólo la primera vez que se importe el documento (si el documento ya existía y se vuelve a importar, no se volverán a realizar losenvíos).
    GlobalId: number;
    Customer: OrderCustomer;
    Notes: string;
}

interface Identifier {
    Id: number;
    Name: string;
}

interface SaleCenter {
    Id: AgoraSaleCenter["Id"];
    Name: AgoraSaleCenter["Name"];
    Location: SaleLocation["Name"];
}

interface OrderCustomer {
    Id: number;
    FiscalName: string;
    Cif: string;
    AccountCode: string;
    Street: string;
    City: string;
    Region: string;
    ZipCode: string;
    CountryCode: string;
    ApplySurcharge: boolean;
}
