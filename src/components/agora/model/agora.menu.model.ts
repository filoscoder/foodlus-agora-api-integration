export interface AgoraMenu {
    Id: number;
    Name: string; // "Menú Merienda";
    VatId: number;
    FamilyId: number;
    ButtonText: string; // "M.Merienda"
    Color: `#${string}`; // "#341122"
    Order: number;
    UseAsDirectSale: boolean;
    SaleableAsMain: boolean;
    Tag: string; // "MM"
    PLU: string; // "0001"
    Barcodes: Barcode[];
    Prices: Price[];
    MenuGroups: MenuGroup[];
}

interface Barcode {
    BarcodeValue: string; // "15998763"
}

interface Price {
    SaleCenterId: number; // "10"
    MainPrice: number; // "9.00"
}

interface MenuGroup {
    Products: Product[]; // "10"
    Name: string;
    MaxItems: number;
    PreparationOrderId: number;
}

interface Product {
    Id: number;
}
