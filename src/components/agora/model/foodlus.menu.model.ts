export interface FoodlusMenuModel {
    id: number;
    name: string;
    type: "menu";
    position: number;
    scheduleLabel: string;
    urlKey: string;
    hasSelection: boolean;
    selectionAvailable: boolean;
    selectionExpired: boolean;
    selectionUpcoming: boolean;
    menuSections: FoodlusMenuSectionModel[];
    price?: number | null;
    shortDescription?: string | null;
    availableFrom?: string | null;
    availableTo?: string | null;
}

export interface FoodlusMenuSectionModel {
    id: number;
    name: string;
    shortDescription?: string;
    image?: string;
    position: number;
    showProductImages: boolean;
    maxSelection: number;
    minSelection: number;
    selectionLabel: string;
    products: FoodlusProductModel[];
}

export interface FoodlusProductModel {
    id: number | string;
    name: string;
    image: string | null;
    position: number;
    price: number;
    public: boolean;
    featured: boolean;
    shortDescription: string;
    description: string;
    parentName: string;
    isAvailable: boolean;
    urlKey: string;
    productOptionsCategories: FoodlusOptionCategoryModel[];
    section_id?: number;
}

export interface FoodlusOptionCategoryModel {
    id: number | string;
    name: string;
    shortDescription: string;
    position: number;
    maxSelection: number | null;
    minSelection: number | null;
    maxSameOptionSelection: number | null;
    isOptional: boolean;
    isVariant: boolean;
    selectionLabel: string;
    productOptions: FoodlusOptionModel[];
}

export interface FoodlusOptionModel {
    id: number | string;
    name: string;
    shortDescription: string;
    position: number;
    isAvailable: boolean;
    price: number;
}
