import { AgoraApiIntegration } from "./agora.repository";
import { AgoraProduct } from "./model/agora.product.model";
import {
    FoodlusMenuModel,
    FoodlusMenuSectionModel,
    FoodlusProductModel
} from "./model/foodlus.menu.model";
import {
    FoodlusTableModel,
    FoodlusZoneModel
} from "./model/foodlus.table.model";

export class AgoraService {
    agoraApi: AgoraApiIntegration;
    constructor() {
        this.agoraApi = new AgoraApiIntegration();
    }

    async getAgoraMenus(): Promise<FoodlusMenuModel[]> {
        const agoraMenus = await this.agoraApi.getMenus();

        return await Promise.all(
            agoraMenus.map(async (agoraMenu): Promise<FoodlusMenuModel> => {
                const { Id, Name, Prices, SaleableAsMain, MenuGroups } =
                    agoraMenu;
                // ! MenuSections should be an Array? Can be more than 1 Menu Section?
                const menuSections = await Promise.all(
                    MenuGroups.map(
                        async (menuGroup): Promise<FoodlusMenuSectionModel> => {
                            const { Name, MaxItems, Products } = menuGroup;
                            // ! Products should be an Array? Can be more than 1 product?
                            const products = await Promise.all(
                                Products.map(
                                    async ({
                                        Id
                                    }): Promise<FoodlusProductModel> => {
                                        // ! [AgoraAPI] > GET PRODUCT DATA by ID
                                        const productsData =
                                            await this.agoraApi.getProducts(Id);
                                        const product = products.length
                                            ? productsData[0]
                                            : ({} as AgoraProduct);
                                        // ! PRICE + VAT?
                                        // ? [AgoraAPI] > GET VAT by ID (`VatId`)
                                        const price = product?.Prices.length
                                            ? Prices[0].MainPrice
                                            : 0;

                                        return {
                                            id: Id,
                                            name: product?.Name,
                                            image: null, // ?
                                            position: 0, // ?
                                            price,
                                            public: false, // ?
                                            featured: false, // ?
                                            shortDescription: "", // ?
                                            description: "", // ?
                                            parentName: "", // ?
                                            isAvailable: false, // ?
                                            urlKey: "", // ?
                                            productOptionsCategories: [] // ?
                                        };
                                    }
                                )
                            );

                            return {
                                id: 1,
                                name: Name,
                                maxSelection: MaxItems,
                                minSelection: 0, // ?
                                position: 0, // ?
                                showProductImages: false, // ?
                                selectionLabel: "", // ?
                                products
                            };
                        }
                    )
                );
                const price = Prices.length ? Prices[0].MainPrice : null;

                return {
                    id: Id,
                    name: Name,
                    hasSelection: true, // ?
                    type: "menu",
                    scheduleLabel: "Tag", // ?
                    position: 1, // ?
                    menuSections,
                    selectionAvailable: SaleableAsMain, // ?
                    selectionExpired: false, // ?
                    selectionUpcoming: false, // ?
                    urlKey: "bdp-menu", // ?
                    price
                };
            })
        );
    }

    async getAgoraTables(): Promise<FoodlusZoneModel[]> {
        const agoraSaleCenters = await this.agoraApi.getSaleCenters();

        const formatedZones = agoraSaleCenters.map((saleCenter) => {
            const { Id, Name: zoneName, SaleLocations } = saleCenter;
            const serviceLocations = SaleLocations.map((saleLocation) => {
                const { Name } = saleLocation;
                const foodlusTable: FoodlusTableModel = {
                    name: Name,
                    code: 0,
                    zoneId: Id,
                    zoneName
                };
                return foodlusTable;
            });
            const zone: FoodlusZoneModel = {
                name: zoneName,
                serviceLocations
            };

            return zone;
        });

        return formatedZones;
    }
}
