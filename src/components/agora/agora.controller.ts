import { OK } from "http-status/lib";
import type { Request, Response, NextFunction } from "express";
import { AgoraService } from "./agora.service";

export const getTables = async (
    _req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const revoService = new AgoraService();
        const result = await revoService.getAgoraTables();

        res.status(OK).json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

export const getFullMenu = async (
    _req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const revoService = new AgoraService();
        const result = await revoService.getAgoraMenus();

        res.status(OK).json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

export const sendOrder = async (
    _req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const revoService = new AgoraService();
        const result = await revoService.getAgoraTables();

        res.status(OK).json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

export const sendOrderAndPayment = async (
    _req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const revoService = new AgoraService();
        const result = await revoService.getAgoraTables();

        res.status(OK).json(result);
    } catch (error) {
        console.log(error);
        next(error);
    }
};
