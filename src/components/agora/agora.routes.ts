import { Router } from "express";
import {
    getTables,
    getFullMenu,
    sendOrder,
    sendOrderAndPayment
} from "./agora.controller";

export const agoraRoutes = Router();

agoraRoutes.get("/tables", getTables);
agoraRoutes.get("/menues", getFullMenu);
agoraRoutes.post("/orders", sendOrder);
agoraRoutes.post("/payments", sendOrderAndPayment);
