import apiRequest from "@/api";
import { AgoraMenu } from "./model/agora.menu.model";
import { AgoraProduct } from "./model/agora.product.model";
import { AgoraSaleCenter } from "./model/agora.tables.model";

type FilterParams =
    | "WorkplacesSummary"
    | "Vats"
    | "PaymentMethods"
    | "PriceLists"
    | "SaleCenters"
    | "Menus"
    | "Products";

export class AgoraApiIntegration {
    async exportByFilter(filter: FilterParams[]) {
        const filterStr = filter.join(",");

        const { data } = await apiRequest.get<AgoraSaleCenter[]>(
            "/export-master",
            {
                params: {
                    filter: filterStr
                }
            }
        );

        return data;
    }

    async getSaleCenters() {
        const { data } = await apiRequest.get<AgoraSaleCenter[]>(
            "/export-master",
            {
                params: {
                    filter: "SaleCenters"
                }
            }
        );

        return data;
    }

    async getMenus() {
        const { data } = await apiRequest.get<AgoraMenu[]>("/export-master", {
            params: {
                filter: "Menus"
            }
        });

        return data;
    }

    async getProducts(id: number) {
        const idQuery = id ? `&where-product-category-id=${id}` : "";
        const { data } = await apiRequest.get<AgoraProduct[]>(
            "/export-master",
            {
                params: {
                    filter: "Products" + idQuery
                }
            }
        );

        return data;
    }

    async import(payload: any) {
        const { data } = await apiRequest.post<any>("/import", payload);

        return data;
    }
}
