# Foodlus Agora API integration
- [Agora Documentation](https://www.agorapos.com/manual/agora-restaurant/guia-integracion-agora-restaurant.pdf )

## Getting Started

#### 1. Install Dependencies

```bash
$ npm i
```

#### 2. Setup dot env file(s)

Place below content at this path:
- `/dotenv/.env.development`

#### 3. Run on dev mode

```bash
$ npm run dev # Running at http://localhost:8080/
```
